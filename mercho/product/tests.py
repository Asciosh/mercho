from django.test import TestCase
from .models import *
from core.models import User,Consumer,Supplier

class testCategory(TestCase):  

    '''test on Category model'''

    def setUp(self):
        self.category1 = Category.objects.create(
            name='Pop',
            slug="pop",
        )
    
    def test_obj(self):
        self.assertTrue(
                isinstance(self.category1, Category), 
                "category1 is not a Category"
            )

    def test_category_slug_is_ok(self):
        self.assertEqual(self.category1.slug, 'pop')
    


class testProduct(TestCase):

    '''test on Product, Review model'''

    def setUp(self):
        category1 = Category.objects.create(
            name='Pop',
            slug="pop",
        )

        user1 = User.objects.create(
            is_consumer="False", 
            is_supplier="True", 
            username='supplier1', 
            first_name="c", 
            last_name="d", 
            email="c@gmail.com",
            )

        user2 = User.objects.create(
            is_consumer="True", 
            is_supplier="False", 
            username='consumer1', 
            first_name="a", 
            last_name="b", 
            email="a@gmail.com",
            )

        supplier1 = Supplier.objects.create(
            user=user1,
            phone_number='1',
            designation='mi',
        )

        consumer1 = Consumer.objects.create(
            user=user2,
            #consumer_genre not used
        )

        self.product1 = Product.objects.create(   # image and created_at not used
            category=category1,   
            name='p1',
            slug='p-1',
            description='desc',
            price=1000,
            added_by=supplier1,   
        )

        review1 = Review.objects.create(  # created_at is not used
            product=self.product1,
            rating=5,
            content='comment',
            created_by=consumer1,
            answer='answer',
        )


    def test_obj(self):
        self.assertTrue(
                isinstance(self.product1, Product), 
                "product1 is not a Product"
            )

    def test_product__str__(self):
        self.assertEqual(str(self.product1), 'p1')  # name == 'p1'

    def test_product_get_display_price(self):
        self.assertEqual(self.product1.get_display_price(), 10)  # integer 1000/100 -> 10

    def test_product_type_and_name(self):
        self.assertRaises(
			TypeError,
			str(self.product1),
			""
		)

        self.product1.name = 'product'  # new name
        self.assertEqual(
			str(self.product1),
			'product',
			"The name's product is not correct"
		)
    
    
    def test_product_review_get_rating(self):
        self.assertEqual(self.product1.get_rating(), 5)  # our product has only one review with rating of 5 -> 5/1 = 5

    def test_product_review_get_rewiew_count(self):  # our product has only one review
        self.assertEqual(self.product1.get_rewiew_count(), 1) 
    


class testReview(TestCase):

    '''test on Review model'''

    def setUp(self):

        category1 = Category.objects.create(
            name='Pop',
            slug="pop",
        )

        user1 = User.objects.create(
            is_consumer="False", 
            is_supplier="True", 
            username='supplier1', 
            first_name="c", 
            last_name="d", 
            email="c@gmail.com",
        )

        supplier1 = Supplier.objects.create(
            user=user1,
            phone_number='1',
            designation='mi',
        )

        product1 = Product.objects.create(   # image and created_at not used
            category=category1,   
            name='p1',
            slug='p-1',
            description='desc',
            price=1000,
            added_by=supplier1,   
        )

        user2 = User.objects.create(
            is_consumer="True", 
            is_supplier="False", 
            username='consumer1', 
            first_name="a", 
            last_name="b", 
            email="a@gmail.com",
        )

        consumer1 = Consumer.objects.create(
            user=user2,
            #consumer_genre not used
        )

        self.review1 = Review.objects.create(  # created_at is not used
            product=product1,
            rating=5,
            content='comment',
            created_by=consumer1,
            answer='answer',
        )


    def test_obj(self):
        self.assertTrue(
                isinstance(self.review1, Review), 
                "review1 is not a Review"
            )

    def test_review_content(self):
        self.assertEqual(
            self.review1.content,
			'comment',
			"The content's review is not correct"
		)