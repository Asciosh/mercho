from django.contrib.auth import views
from django.urls import path

from core.views import frontpage, shop, myaccount, edit_myaccount
from product.views import product
from .views import register, consumer_register, supplier_register, ProductDeleteView ,add_product, product_success, consumer_success, supplier_success  #ProductUpdateView

urlpatterns = [
    path('', frontpage, name='frontpage'),
    #path('signup/', signup, name='signup'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('login/', views.LoginView.as_view(template_name='core/login.html'), name='login'),
    path('myaccount/', myaccount, name='myaccount'),
    path('myaccount/edit/', edit_myaccount, name='edit_myaccount'),
    path('shop/', shop, name='shop'),
    path('shop/<slug:slug>/', product, name='product'), #dynamic url


    path('register/', register, name='register'),
    path('consumer_register/', consumer_register.as_view(success_url="success/"), name='consumer_register'), #success_url="success/"
    path('supplier_register/', supplier_register.as_view(success_url="success/"), name='supplier_register'),
    path('consumer_register/success/', consumer_success, name='consumer_success'),
    path('supplier_register/success/', supplier_success, name='supplier_success'),


    # supplier 
    path('add_product/', add_product, name='add_product'),
    path('add_product/success/', product_success, name='product_success'),


    path('myaccount/<pk>/delete/', ProductDeleteView.as_view()),
    #path('myaccount/<pk>/update/', ProductUpdateView.as_view()),
]