from django.contrib import admin

from .models import User, Consumer, Supplier


class NewConsumerAdmin(admin.ModelAdmin):
    list_display = [
    'user_id',
    ]
    search_fields = [
    'user_id',
    ]

admin.site.register(Consumer, NewConsumerAdmin)


class NewSupplierAdmin(admin.ModelAdmin):
    list_display = [
    'user_id',
    'phone_number',
    'designation',
    ]
    search_fields = [
    'user_id',
    ]

admin.site.register(Supplier, NewSupplierAdmin)


class NewUserAdmin(admin.ModelAdmin):
    list_display = [
    'id',
    'is_superuser',
    'is_staff',
    'is_supplier',
    'is_consumer',
    ]
    search_fields = [
    'id',
    'is_superuser',
    'is_staff',
    'is_supplier',
    'is_consumer',
    ]

admin.site.register(User, NewUserAdmin)