from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction

from .models import User, Consumer, Supplier
from product.models import Category, Product


class ConsumerSignUpForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(required=True)

    genre = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all(),
        widget=forms.CheckboxSelectMultiple,
        required=True
    )

    class Meta(UserCreationForm.Meta):
        model = User

    #all operations at the same time
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_consumer = True
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.email = self.cleaned_data.get('email')
        user.save()
        consumer = Consumer.objects.create(user=user)
        consumer.genre.add(*self.cleaned_data.get('genre'))
        consumer.save()
        return user


class SupplierSignUpForm(UserCreationForm):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    email = forms.CharField(required=True)
    phone_number = forms.CharField(required=True)
    designation = forms.CharField(required=True)

    class Meta(UserCreationForm.Meta):
        model = User

    #all operations at the same time
    @transaction.atomic
    def save(self):
        user = super().save(commit=False)
        user.is_supplier = True
        user.is_staff = False
        user.first_name = self.cleaned_data.get('first_name')
        user.last_name = self.cleaned_data.get('last_name')
        user.email = self.cleaned_data.get('email')
        user.save()
        supplier = Supplier.objects.create(user=user)
        supplier.phone_number = self.cleaned_data.get('phone_number')
        supplier.designation = self.cleaned_data.get('designation')
        supplier.save()
        return user


class ProductForm(forms.ModelForm):
    slug = forms.SlugField()
 
    class Meta:
        model = Product
        fields = ['category', 'name', 'slug', 'description', 'price', 'image'] #added_by