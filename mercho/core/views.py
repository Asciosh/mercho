from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.views.generic.edit import DeleteView  #UpdateView

import random

from .forms import ConsumerSignUpForm, SupplierSignUpForm, ProductForm
from .models import User, Consumer, Supplier
#from .decorators import consumer_required, supplier_required
from order.models import Order
from product.models import Product, Category #Review


def register(request):
    return render(request, 'core/register.html')

class consumer_register(CreateView):
    model = User
    form_class = ConsumerSignUpForm
    template_name = 'core/consumer_register.html'

    def validation(self, form):
        user=form.save()
        login(self.request,user)
        return redirect('/')

class supplier_register(CreateView):
    model = User
    form_class = SupplierSignUpForm
    template_name = 'core/supplier_register.html'

    def validation(self, form):
        user=form.save()
        login(self.request,user)
        return redirect('/')


def frontpage(request):
    products = Product.objects.all()[0:8] # new entries

    if not request.user.is_authenticated or request.user.is_supplier:

        return render(request, 'core/frontpage.html', {'products': products})
    
    else: # consumer

        #orders = Order.objects.all() #all
        #orders = orders.filter(consumer=request.user.id)
        

        all_products = list(Product.objects.all()) 
        max_products = len(all_products)

        random_products = random.sample(all_products, max_products)[0:8]  # max 8 products
        
        consumer = Consumer.objects.get(user = request.user)
        categories = consumer.genre.all() # Consumer favorite genres

        context = {
            'products': products,
            'random_products' : random_products,
            'categories': categories,
            #'orders': orders,
        }

        return render(request, 'core/frontpage.html', context)


@login_required
def myaccount(request):
    if request.user.is_consumer:  #consumer account
        orders = Order.objects.all() #all
        orders = orders.filter(consumer=request.user.id)
        
        consumer = Consumer.objects.get(user=request.user)
        categories = consumer.genre.all()

        context = {
            'orders': orders,
            #'consumer': consumer,
            'categories': categories,
        }

    else:   #supplier account
        supplier  = Supplier.objects.get(user=request.user)

        products = Product.objects.all()
        products = Product.objects.filter(added_by=supplier)


        sort_by = request.GET.get('created_at', '')
        if sort_by == "newest":
            products = products.order_by('-created_at')
        elif sort_by == "oldest":
            products = products.order_by('created_at')

        sort_by = request.GET.get('price', '')
        if sort_by == "l2h":
            products = products.order_by('price')
        elif sort_by == "h2l":
            products = products.order_by('-price')


        context = {
            'supplier':supplier,
            'products': products,
        }

    return render(request, 'core/myaccount.html', context)
    

@login_required
def edit_myaccount(request):
    context = {}

    if request.user.is_supplier:
        supplier = Supplier.objects.get(user=request.user)
        context = {
            'supplier': supplier,
        }
    
    #else: 
    #    consumer = Consumer.objects.get(user=request.user)
    #    categories = consumer.genre.all()
    #    context={
    #        'categories': categories,
    #    }

    if request.method == 'POST':
        user = request.user
        user.first_name = request.POST.get('first_name')
        user.last_name = request.POST.get('last_name')
        user.email = request.POST.get('email')
        user.username = request.POST.get('username')

        if request.user.is_supplier: 
            supplier.phone_number = request.POST.get('phone_number')
            supplier.designation = request.POST.get('designation')
            supplier.save()

        #else:
        #    categories = request.POST.get('categories')  # consumer.categories
        
        user.save()

        return redirect('myaccount')

    return render(request, 'core/edit_myaccount.html', context)


def shop(request):
    categories = Category.objects.all() #all
    products = Product.objects.all()

    active_category = request.GET.get('category', '')

    if active_category: #display only the products of the active_category 
        products = products.filter(category__slug=active_category)
        #products = products.order_by('-created_at',)
    

    #sort_by = request.GET.get('sort', 'l2h') #?sort=l2h 
    sort_by = request.GET.get('price', '')
    if sort_by == "l2h":
        products = products.order_by('price')
    elif sort_by == "h2l":
        products = products.order_by('-price')


    #sort_by2 = request.GET.get('rating', '')
    #if sort_by2 == "l2h":
    #    print("a")
        #products = products.order_by('reviews') no
        #reviews = reviews.order_by('rating')
    #elif sort_by2 == "h2l":
    #    print("a")
        #products = products.order_by('-reviews') no
        #reviews = reviews.order_by('-rating') 

    
    query = request.GET.get('query', '')  #input name query on shop.html, default empty  

    if query:
        products = products.filter(Q(name__icontains=query) | Q(description__icontains=query) | Q(category__name__icontains=query)) #icontains not sensitive, search on name and description of the product


    context = {
        'categories': categories,
        'products':products,
        'active_category':active_category,
    }

    return render(request, 'core/shop.html', context)


def add_product(request):
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
 
        if form.is_valid():
            
            Product = form.save(commit=False)

            supplier  = Supplier.objects.get(user=request.user)

            Product.added_by = supplier
            Product.save()
               
            form.save()
            return redirect('success/')
    else:
        form = ProductForm()
    
    return render(request, 'core/add_product.html', {'form': form})


def product_success(request):
    return render(request, 'core/product_success.html')

def consumer_success(request):
    return render(request, 'core/consumer_success.html')

def supplier_success(request):
    return render(request, 'core/supplier_success.html')


class ProductDeleteView(DeleteView):
    # specify the model you want to use
    model = Product
     
    # can specify success url
    # url to redirect after successfully
    # deleting object
    success_url ="/"
     

'''
class ProductUpdateView(UpdateView):
    # specify the model you want to use
    model = Product
  
    # specify the fields
    fields = [
        "price",
    ]
  
    # can specify success url
    # url to redirect after successfully
    # updating details
    success_url ="core/product_success.html"
'''

'''
def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)

        if form.is_valid():
            user = form.save() #user created

            login(request, user)

            return redirect('/')
    else: 
        form = SignUpForm()

    return render(request, 'core/signup.html', {'form': form})
'''
